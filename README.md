# digiccy-tr-app

这是一个虚拟币自动量化交易工具，作为桌面应用程序运行在Windows平台。内置交易策略，启动后，可全天24小时运行在云服务器上，自动执行买入、买出操作。虚拟币价格瞬息万变，对于很小的价格波动，机器也可以监测到，价格越是波动频繁，止盈下单操作频率就越高，让利益最大化。最大的优点就是可以规避人的情绪波动，而产生错误的判断，也避免了由于价格波动，让人陷入焦虑的情绪。是基于火币网交易API开发的桌面应用程序。它更多的是面向不懂编程语言的用户，让他们也可以轻松进行量化交易。技术上基于Electron，它是一个可以使用 JavaScript，HTML 和 CSS 构建跨平台的桌面应用程序的框架。

● 开发环境：VSCode、Nodejs

● 技术栈：Electron、Vue、ElementUI、JS/CSS/HTML

● 项目模块：包含了任务管理、报表、行情分析、任务监视、设置等模块

        

![](http://static.giteasy.cn/ect01.jpg)
![](http://static.giteasy.cn/ect02.gif)
![](http://static.giteasy.cn/ect03.gif)
## Project setup
```
npm install

```
> 安装依赖时，可能会因为网络原因，等待时间较长。（尤其是Electron依赖）

### Compiles and hot-reloads for development
```
npm run electron:serve
```

### Compiles and minifies for production
```
npm run electron:build
```
