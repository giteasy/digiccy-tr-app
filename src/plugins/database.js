import Vue from 'vue';
// import crypto from 'crypto'
import Datastore from 'nedb-promises';
//https://changkun.us/archives/2017/03/217/
//直接使用require('electron')报错，无法获取electron，原因为是webpack打包时默认输出路径问题路径问题，这里暂时使用window.require代替
//https://www.cnblogs.com/wonyun/p/10991984.html
const { remote } =  window.require('electron');

const basePath = remote.app.getPath('userData');

console.log('程序数据存储：' + basePath)

const db = new Datastore({
    autoload:true,
    timestampData:true,
    filename:basePath
})

Vue.prototype.$db = db;
