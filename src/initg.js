import db from './plugins/datastore';
import symbols from './huobi-symbol'
global.CONFIG = {
    huobi:{
       
    }
}
//不翻墙：api.huobi.de.com
// 翻墙：api.huobi.pro
// aws用户：api-aws.huobi.pro
global.CONFIG.huobi.httpBaseUrl = 'https://api.huobi.de.com';
global.CONFIG.huobi.wsUrl = 'wss://api.huobi.de.com/ws';

loadHuobiKey();
function loadHuobiKey(){
  db.key.findOne().then((doc)=>{
    if(doc){
      global.CONFIG.huobi.accessKey = doc.accessKey;
      global.CONFIG.huobi.secretKey = doc.secretKey;
      if(doc.host && doc.host.length > 0){
        global.CONFIG.huobi.httpBaseUrl = 'https://' + doc.host;
        global.CONFIG.huobi.wsUrl = 'wss://'+doc.host+'/ws';
      }
     
    }
  });
}
loadSymbols();
function loadSymbols(){
    let symbolMap = new Map();
    for(let i in symbols){
        let item = symbols[i];
        symbolMap.set(item.symbol,item);
    }
    global.CONFIG.huobi.symbols = symbols;
    global.CONFIG.huobi.symbolMap = symbolMap;
}


global.CONFIG.publicKey = `-----BEGIN PUBLIC KEY-----
MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAIXcWkQ67+hP5R25/29VcREa5SbEfq5G
wDQPWsgsjlBLr4dwOzXhAhOZd9mNX3yDj3MfUGY+TFTAxGBRqSSyUU0CAwEAAQ==
-----END PUBLIC KEY-----`