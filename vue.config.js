module.exports = {
  lintOnSave: false,
  pages: {
    index: 'src/pages/index/main.js',
    // 当使用只有入口的字符串格式时，
    // 模板会被推导为 `public/subpage.html`
    // 并且如果找不到的话，就回退到 `public/index.html`。
    // 输出文件名会被推导为 `subpage.html`。
    displayer: 'src/pages/displayer/main.js',
    trade: 'src/pages/trade/main.js',
    market: 'src/pages/market/main.js',
    update: 'src/pages/update/main.js',
    about: 'src/pages/about/main.js',
  },
  pluginOptions: {
    electronBuilder: {
      builderOptions: {
        "appId": "cn.ectapp.ectapp",
        "copyright":"Copyright © 2020",//版权信息
        "directories":{
           "output":"./dist_electron"//输出文件路径
        },
        "win":{//win相关配置
            "icon":"./favicon.ico",//图标，当前图标在根目录下，注意这里有两个坑
            "target": [
                {
                    "target": "nsis",//利用nsis制作安装程序
                    "arch": [
                        // "x64"//64位
                        "ia32"//32位
                    ]
                }
            ]
        },
        "nsis": {
          "oneClick": false, // 是否一键安装
          "allowElevation": true, // 允许请求提升。 如果为false，则用户必须使用提升的权限重新启动安装程序。
          "allowToChangeInstallationDirectory": true, // 允许修改安装目录
          "installerIcon": "./favicon.ico",// 安装图标
          "uninstallerIcon": "./favicon.ico",//卸载图标
          "installerHeaderIcon": "./favicon.ico", // 安装时头部图标
          "createDesktopShortcut": true, // 创建桌面图标
          "createStartMenuShortcut": true,// 创建开始菜单图标
          "shortcutName": "ECT", // 图标名称
      },
      publish: [
        {
         provider: "generic",
         channel: "latest",
         url: "http://update.giteasy.cn/update/"
        }
      ]
      }
    }
  }
};


//打包
//https://www.jianshu.com/p/dfcf2a6a497c
// https://www.jianshu.com/p/1dbb96bc8f37
//https://www.electron.build/